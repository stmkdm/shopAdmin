import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
// import './plugins/element.js'
import './assets/css/global.css'
import './assets/fonts/iconfont.css'
import axios from 'axios'
import TreeTable from 'vue-table-with-tree-grid'
import NProgress from 'nprogress'


//配置穷请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
//在request拦截器中，展示进度条  NProgress.start()

axios.interceptors.request.use(config => {
  //为headers添加authorization字段
  NProgress.start()
  config.headers.Authorization = window.sessionStorage.getItem('token')
  //最后必须返回config
  return config
})
//在response拦截器中，影藏进度条  NProgress.done()


axios.interceptors.response.use(config => {
  NProgress.done()
  return config
})

Vue.prototype.$http = axios


Vue.config.productionTip = false

Vue.component('tree-table', TreeTable)

//全局过滤器 时间格式化
Vue.filter('dateFormat', function (time) {
  const dt = new Date(time)
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '')
  const hh = dt.getHours()
  const mm = dt.getMinutes()
  const ss = dt.getSeconds()

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`

})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
