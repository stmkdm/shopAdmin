import Vue from 'vue'
import VueRouter from 'vue-router'

const Login = () => import(/* webpackChunkName: "group-foo" */ '../components/Login.vue')
const Home = () => import(/* webpackChunkName: "group-foo" */ '../views/Home.vue')
const Welcome = () => import(/* webpackChunkName: "group-foo" */ '../components/Welcome.vue')
const Users = () => import(/* webpackChunkName: "group-foo" */ '../components/user/Users.vue')
const Rights = () => import(/* webpackChunkName: "group-foo" */ '../components/power/Rights.vue')
const Roles = () => import(/* webpackChunkName: "group-foo" */ '../components/power/Roles.vue')
const Goods = () => import(/* webpackChunkName: "group-foo" */ '../components/good/Goods')
const Params = () => import(/* webpackChunkName: "group-foo" */ '../components/good/Params')
const Cates = () => import(/* webpackChunkName: "group-foo" */ '../components/good/Cates')
const Orders = () => import(/* webpackChunkName: "group-foo" */ '../components/order/Orders')
const Reports = () => import(/* webpackChunkName: "group-foo" */ '../components/report/Reports')
// import Login from '../components/Login.vue'
// import Home from '../views/Home.vue'
// import Welcome from '../components/Welcome.vue'
// import Users from '../components/user/Users.vue'
// import Rights from '../components/power/Rights.vue'
// import Roles from '../components/power/Roles.vue'
// import Goods from '../components/good/Goods'
// import Params from '../components/good/Params'
// import Cates from '../components/good/Cates'
// import Orders from '../components/order/Orders'
// import Reports from '../components/report/Reports'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

const routes = [
  {path: '/login', component: Login},
  {path: '/', redirect: '/login'},
  {
    path: '/home', component: Home, redirect: '/welcome', children: [
      {path: '/welcome', component: Welcome},
      {path: '/users', component: Users},
      {path: '/rights', component: Rights},
      {path: '/roles', component: Roles},
      {path: '/goods', component: Goods },
      {path: '/params', component: Params },
      {path: '/categories', component: Cates },
      {path: '/orders', component: Orders },
      {path: '/reports', component: Reports },
    ]
  }
]

const router = new VueRouter({
  routes
})

//路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  if (to.path === '/home') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})




export default router
